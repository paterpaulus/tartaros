var assert = require('assert');

var tartaros = require('../tartaros.js');

describe('tartaros', function () {
  it('Works with strings', function () {
    var validate = tartaros('String');

    assert.equal(validate('foo'), true);
    assert.equal(validate(''), true);
    assert.equal(validate(String(5)), true);
    assert.equal(validate((42).toString()), true);
    assert.equal(validate({}), false);
    assert.equal(validate([]), false);
    assert.equal(validate(42), false);
  });

  it('Works with numbers', function () {
    var validate = tartaros('Number');

    assert.equal(validate(5), true);
    assert.equal(validate(4.2), true);
    assert.equal(validate(Infinity), true);
    assert.equal(validate('foo'), false);
    assert.equal(validate([]), false);
  });

  it('Works with booleans', function () {
    var validate = tartaros('Bool');

    assert.equal(validate(true), true);
    assert.equal(validate(false), true);
    assert.equal(validate('hey'), false);
    assert.equal(validate([]), false);
  });

  it('Works with null', function () {
    var validate = tartaros('Null');
    
    assert.equal(validate(null), true);
    assert.equal(validate(undefined), false);
    assert.equal(validate(0), false);
    assert.equal(validate(''), false);
    assert.equal(validate([]), false);
  });

  it('Works with undefined', function () {
    var validate = tartaros('Undefined');

    assert.equal(validate(undefined), true);
    assert.equal(validate(), true);
    assert.equal(validate(null), false);
    assert.equal(validate(''), false);
    assert.equal(validate(0), false);
  });

  it('Works with everything but undefined', function () {
    var validate = tartaros('Defined');

    assert.equal(validate(undefined), false);
    assert.equal(validate(), false);
    assert.equal(validate(null), true);
    assert.equal(validate(''), true);
    assert.equal(validate(0), true);
    assert.equal(validate([]), true);
    assert.equal(validate({}), true);
  });

  it('Defined can be used to check whether a key is set in an object', function () {
    var validate = tartaros({
      bestKeyEver: 'Defined'
    });

    assert.equal(validate({}), false);
    assert.equal(validate({ badKey: 42 }), false);
    assert.equal(validate({ bestKeyEver: 42 }), true);
  });
});
