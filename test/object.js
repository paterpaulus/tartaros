var assert = require('assert');

var tartaros = require('../tartaros.js');

describe('tartaros', function () {
  it('Handles null well', function () {
    var isValid = tartaros({
      superKey: 'String'
    });

    assert.equal(isValid(null), false);
  });

  it('Can has an object as the schema', function () {
    var validate = tartaros({
      username: 'String',
      isAwesome: 'Bool'
    });

    assert.equal(validate({
      username: 'Zaphod BB',
      isAwesome: true
    }), true);
  });

  it('Does not match objects if the keys are not exactly the same', function () {
    var validateEmpty = tartaros({});
    var validate = tartaros({
      foo: 'Number',
      bar: 'String'
    });

    assert.equal(validateEmpty({}), true);
    assert.equal(validateEmpty({ foo: 'bar' }), false);
    assert.equal(validate({}), false);
    assert.equal(validate({ foo: 42 }), false);
    assert.equal(validate({
      foo: 42,
      bar: 'hey'
    }), true);
    assert.equal(validate({
      foo: 42,
      bar: 'hey',
      blub: 'blab'
    }), false);
  });

  it('Does not match if any of the properties does not match', function () {
    var validate = tartaros({
      name: 'String',
      age: 'Number'
    });

    assert.equal(validate({
      name: 42,
      age: 'Arthur Dent'
    }), false);
  });

  it('Matches nested objects', function () {
    var validate = tartaros({
      city: 'String',
      street: {
        name: 'String',
        address: 'Number'
      }
    });

    assert.equal(validate({
      city: 'VogsTown',
      street: {
        name: 'VogStreet',
        address: 42
      }
    }), true);
  });
});
