var assert = require('assert');

var tartaros = require('../tartaros.js');

describe('tartaros', function () {
  it('Matches Arrays', function () {
    var validate = tartaros([ 'String' ]);

    assert.equal(validate([
      'Hello, World!',
      'foo',
      'bar'
    ]), true);
    assert.equal(validate([]), true);
  });

  it('Does not match arrays that contain any item of the wrong type', function () {
    var validate = tartaros([ 'Number' ]);
    
    assert.equal(validate([ 1, 2, 3, 4, true ]), false);
    assert.equal(validate([ '42' ]), false);
    assert.equal(validate([ [] ]), false);
  });

  it('Can match nested arrays', function () {
    var validate = tartaros([ [ 'Bool' ] ]);

    assert.equal(validate([
      [ true, false ],
      [ false, true ],
      []
    ]), true);
  });
});
