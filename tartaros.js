'use strict';

module.exports = function tartaros (schema) {
  return function _validate (x) {
    return validate(schema, x);
  };
};

function validate (schema, x) {
  if (typeof schema === 'string') {
    return validateBasic(schema, x);
  } else if (schema instanceof Array) {
    return validateArray(schema, x);
  } else {
    return validateObject(schema, x);
  }
};

function validateObject (schema, x) {
  var schemaKeys = Object.keys(schema);
  var i;
  var n;
  
  if (typeof x !== 'object' || x === null) {
    // necessary because typeof null === 'object'
    return false;
  } else if (schemaKeys.length !== Object.keys(x).length) {
    // if there are not even the same numbers of keys
    // there wont be the same keys
    return false;
  } else {
    for (i = 0, n = schemaKeys.length; i < n; i++) {
      if (!validate(schema[schemaKeys[i]], x[schemaKeys[i]])) {
        return false;
      }
    }

    // if we got that far, there are the same number of keys
    // in x and the schema, and also the schemas of every key in the schema
    // match the corresponding properties in x
    // that means that there are the same keys in schema and x
    // and also that all of them match
    return true;
  }
}

function validateArray (schema, x) {
  var i;
  var n;

  if (schema.length !== 1) {
    return false;
  } else if (!(x instanceof Array)) {
    return false;
  } else {
    // if any of x's items does not match the schema,
    // return false
    for (i = 0, n = x.length; i < n; i++) {
      if (!validate(schema[0], x[i])) {
        return false;
      }
    }
    
    // if we got that far, all items matched the schema
    // or the array was empty
    return true;
  }
}

// This handles cases where the schema is just a string.
function validateBasic (schema, x) {
  if (schema === 'String') {
    return typeof x === 'string';
  } else if (schema === 'Number') {
    return typeof x === 'number';
  } else if (schema === 'Bool') {
    return typeof x === 'boolean';
  } else if (schema === 'Null') {
    /*
    typeof null === 'object'
    That means we have to do a direct comparison.
    We use the strict comparison because the weak one would return true for undefined too.
    */
    return x === null;
  } else if (schema === 'Undefined') {
    return typeof x === 'undefined';
  } else if (schema === 'Defined') {
    return typeof x !== 'undefined';
  } else {
    return false;
  }
};
